#!/usr/bin/env bash

# This program builds an image with kaniko. This program emulates GitLab CI
# environment to test image building in the developer host machine.
# Produced image will contain an installed clang.

set -e

# Pinned image name of Kaniko for build. Using debug container that contains
# shell.
kaniko_image_name="gcr.io/kaniko-project/executor:v1.6.0-debug"

# When we use sudo to start docker, the developer user's PATH will not
# effective.
declare docker_command
docker_command="$(command -v docker)"
readonly docker_command

# Hide complexity of docker calling from different user environment.
function call_docker() {
    local sub_command=("${@}")
 
    if [[ "$(id --name --groups)" == *"docker"* ]]                            \
       ||                                                                     \
       [[ -n ${DOCKER_HOST} ]]; then
        "${docker_command}" "${sub_command[@]}"
    else
        sudo "${docker_command}" "${sub_command[@]}"
    fi
}

# To determine the project root directory
declare script_dir
script_dir=$(readlink --canonicalize-existing --verbose                       \
    "$(dirname "$(command -v "$0")")")
readonly script_dir

declare repo_root_dir
repo_root_dir=$(
    set +e
    cd "${script_dir}"
    git rev-parse --show-toplevel
)
readonly repo_root_dir

declare clone_of_certs_dir
clone_of_certs_dir=$(mktemp --directory --quiet)
readonly clone_of_certs_dir

function clean_up()
{
    rm --recursive --force "${clone_of_certs_dir}"
}

trap clean_up EXIT

# Clone certificate store to be able to reach/modify by image builder process.
cp --archive "/etc/ssl/certs" "${clone_of_certs_dir}/certs"

# Assemble command that builds the image by Kaniko.
# The command finally will be a "docker run" command. The parameters before
# kaniko image name will be passed to docker. Parameters after kaniko image
# name will be passed to our kaniko executor wrapper. Paths that the kaniko
# executor handles, must be valid inside of the executor container.
# Root directory of this repository will be mounted as workspace directory
# into the running container.
# Certificate store of host operating system will be mounted during run of
# docker.

# Workspace directory name inside in the container.
declare -r workspace_dir="/tmp/workspace"

# Create environment that normally GitLab CI does for its jobs.
declare -r environment_file="/tmp/kaniko.envfile"
cat <<EOF > "${environment_file}"
CI_PROJECT_DIR=${workspace_dir}
CI_REGISTRY_USER=a_user
CI_REGISTRY=a_registry
CI_REGISTRY_PASSWORD=secret
EOF

# It is required for kaniko executor, but not used when upload is not required.
declare -r image_destination="clang:focal-12"

# Repository root mounted to the container. The mounted directory is pointed by
# $CI_PROJECT_DIR environment variable in the container.
declare -ar build_command=("run"
    "--mount" "type=bind,src=${repo_root_dir},dst=${workspace_dir}"
    "--mount" "type=bind,src=${clone_of_certs_dir}/certs,dst=/etc/ssl/certs"
    "--env-file" "${environment_file}"
    "--entrypoint" ""
    "--rm" "--interactive" "--tty"
    "${kaniko_image_name}"
    "${workspace_dir}/CI/scripts/build_image_in_kaniko.sh"
    "--dockerfile" "${workspace_dir}/Dockerfile"
    "--destination" "${image_destination}"
    "--context" "dir://${workspace_dir}"
    "--no-push" "--use-new-run"                                                   \
    "--tarPath" "${workspace_dir}/clang.image.tgz"
)

# Call kaniko to build the image
call_docker "${build_command[@]}"

echo "Clang image succesfully created here: '${repo_root_dir}/clang.image.tgz'."
