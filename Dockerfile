ARG BASE_IMAGE_NAME="ubuntu:20.04"

ARG USER_GROUP
ARG USER_ID
ARG USER_LOGIN_NAME
ARG CLANG_VERSION

FROM ${BASE_IMAGE_NAME}

ARG USER_GROUP=${USER_GROUP:-100}
ARG USER_ID=${USER_ID:-1000}
ARG USER_LOGIN_NAME=${USER_LOGIN_NAME:-developer}
ARG CLANG_VERSION=${CLANG_VERSION:-12}

USER root

# Update OS
RUN                                                                           \
    apt-get update --quiet                                                         && \
    apt-get --yes --quiet upgrade

# Prepare environment to be able to install clang components.
RUN                                                                           \
    apt-get install --quiet --yes software-properties-common wget          && \
    update-ca-certificates --verbose --fresh

RUN                                                                           \
    wget --output-document="/tmp/llvm.sh" "https://apt.llvm.org/llvm.sh"   && \
    chmod u+x "/tmp/llvm.sh"                                               && \
    /tmp/llvm.sh ${CLANG_VERSION}                                          && \
    rm --recursive --force /tmp/tmp* /tmp/llvm.sh

# Install clang-tidy clangsa and clang developer tools.
RUN                                                                           \
    apt-get install --quiet --yes "clang-tidy-${CLANG_VERSION}"               \
        "clang-tools-${CLANG_VERSION}" "libclang-cpp${CLANG_VERSION}-dev"     \
        "libclang1-${CLANG_VERSION}" "libclang-common-${CLANG_VERSION}-dev"

# Update alternatives system to use specific clang version as te default clang
COPY "./clang-manage-alternatives" "/tmp/ua/"
RUN                                                                           \
    /tmp/ua/uagen.sh -v "${CLANG_VERSION}"                                 && \
    /tmp/ua/reg-clang.sh                                                   && \
    rm --recursive --force /tmp/ua

# Create "developer user" identity to create files with user rights according
# to the mounted host filesystem.
RUN                                                                           \
    useradd --create-home --home /home/${USER_LOGIN_NAME} --gid=${USER_GROUP} \
        --uid=${USER_ID} --shell=/bin/bash ${USER_LOGIN_NAME}

ENV DEVELOPER_USER_ID=${USER_ID}
USER ${DEVELOPER_USER_ID}
WORKDIR /home/${USER_LOGIN_NAME}
