#!/busybox/ash
# shellcheck shell=dash

# Shallow shell script that calls kaniko executor inside in a running kaniko
# container.

# Input environment variables. These are set by GitLab CI engine, and this
# script uses.
#
# CI_PROJECT_DIR=${workspace_dir}
# CI_REGISTRY_USER=a_user
# CI_REGISTRY=a_registry
# CI_REGISTRY_PASSWORD=secret
#
# Input parameters. Parameters that kaniko executor understand. Passed to it
# without dealing with them here.

set -e

# Create authentication file to access container registry of GitLab project.
# The file location is fixed by kaniko container.
mkdir -p "/kaniko/.docker"

user_credential=$(echo -n "${CI_REGISTRY_USER}:${CI_REGISTRY_PASSWORD}" | base64)
readonly user_credential

# Assemble authentication controller config file of docker.
# It is necessary to push created image into the container registry of the
# project.
cat <<EOF > "/kaniko/.docker/config.json"
{
    "auths": {
        "${CI_REGISTRY}": {
            "auth": "${user_credential}"
        }
    }
}
EOF

# Execute kaniko executor to create image.
/kaniko/executor "$@"
