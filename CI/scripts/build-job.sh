#!/busybox/ash
# shellcheck shell=dash

# This program is runing by GitLab CI. It builds clang image and push it to the
# container registry of this project.

set -e

"${CI_PROJECT_DIR}/CI/scripts/build_image_in_kaniko.sh"                       \
    --dockerfile "${CI_PROJECT_DIR}/Dockerfile"                               \
    --context "dir://${CI_PROJECT_DIR}"                                       \
    --destination ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
