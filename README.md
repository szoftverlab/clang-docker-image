# Clang docker image

Creates docker image based on ubuntu for C/C++ development with clang toolset.

## Description

Created docker image can be used as base image of others that needs clang
tools. It can be a "layer" in the middle of your development software stack.

It adds a new user to the created ubuntu/debian image. It is useful if you plan
to store result files in a directory that is mounted from the developer host
machine. If you save files with this additional user then the stored files
staying manageable by the user who runs the container.

## To build a new image

Dockerfile can be parametrized. User can give the following parameters
during image build.

- BASE_IMAGE_NAME, the default is "ubuntu:20.04".
- USER_GROUP, the default is 100. This is the users group on ubuntu.
- USER_ID, the default is 1000. This is the first available user id in ubuntu.
- USER_LOGIN_NAME, the default is developer.
- CLANG_VERSION, the default is 12.

### Build image with naked docker

Run your build command in the root directory of clone of this repository.

#### Image builder example commands

If you are a developer, and want to produce binaries for ubuntu bionic with
clang 11 in your developer machine:

```sh
sudo docker build \
    --build-arg="USER_GROUP=$(id --group)" \
    --build-arg="USER_ID=$(id --user)" \
    --build-arg="USER_LOGIN_NAME=$(id --user --name)" \
    --build-arg="CLANG_VERSION=11" \
    --build-arg="BASE_IMAGE_NAME=ubuntu:18.04" \
    --tag="clang-dev:bionic-11" -f Dockerfile .
```
Try your new image:

```sh
sudo docker run --rm --interactive --tty clang-dev:bionic-11 clang++ --version
```

If you want to create an image for your CI, and defaults of the image is
acceptable for you:

```sh
sudo docker build --build-arg="USER_ID=4400" -build-arg="USER_LOGIN_NAME=bot" \
    --tag="clang-dev" -f Dockerfile .
```

### Build image with a simple command

There is a helper script that builds clang image with default build arguments
with one line.

```sh
scripts/build_image.sh
```

The command above creates image as an archive in the repo root directory,
named "clang.image.tgz". It can be imported by
["docker import"](https://docs.docker.com/engine/reference/commandline/import/)
command. At this moment, the build script leaves Dockerfile arguments in its
default values.

### Pre-built image in CI

Image is built in GitLab CI/CD. The produced image stored in the container
registry of this project. CI built the container with default parameters of
Dockerfile.

Clang image can be pulled with the following command:

```sh
sudo docker pull registry.gitlab.com/szoftverlab/clang-docker-image
```

This repository contains a helper script what is using to build image in a
[kaniko](https://github.com/GoogleContainerTools/kaniko) container. (The kaniko
version pinned to the "v1.6.0" in the builder script.
[Kaniko release information here](https://github.com/GoogleContainerTools/kaniko/releases))
